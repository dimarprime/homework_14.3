﻿
#include <iostream>
#include <string>

int main()
{
    std::string name( "Dmitriy");
    std::cout << "Name:" << "\n";
    std::cout << name << "\n";
    std::cout << "The name length in letters:" << "\n";
    std::cout << name.length() << "\n";
    std::cout << "The first letter in name:" << "\n";
    std::cout << name[0] << "\n";
    std::cout << "The last letter in name:" << "\n";
    std::cout << name[name.length() - 1] << "\n";

}
